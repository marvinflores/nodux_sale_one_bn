// Copyright (c) 2016, NODUX and contributors
// For license information, please see license.txt

frappe.ui.form.on('Sales Invoice One', {
	onload: function (frm) {
		var me = this;
		var nom_fact = "";
		// if (frm.doc.items==0) {

		// }
		if (frm.doc.nota_venta == 1) {
			console.log(frm.doc.naming_series);
			frm.set_value("naming_series", "NV-002-.#########");
		}
		frappe.db.get_value("Punto de Venta", {}, "sequence_note", function (r) {
			console.log();
		})

		frappe.db.get_value("Company", {
			"company_name": frm.doc.company
		}, "vat_number", function (r) {
			nom_fact = r.vat_number + "_" + frm.doc.name;
			date = frm.doc.posting_date;
			year = frm.doc.posting_date.slice(0, -6);
			month = frm.doc.posting_date.slice(5, -3);
			company = frm.doc.company;
			company = company.replace(/\s/g, "_");
			company = company.toLowerCase();
		})



		frm.refresh_fields();
		if (frm.doc.status == 'Draft' && frm.doc.docstatus == '0') {
			frm.call({
				method: "nodux_sale_one.nodux_sale_one.doctype.sales_invoice_one.sales_invoice_one.get_series",
				args: {
					'nota_venta': 0,
				},
				callback: function (r) {
					if (!r.exc) {
						frm.set_value("naming_series", r.message);
					}
				}
			});
			frm.call({
				method: "nodux_sale_one.nodux_sale_one.doctype.sales_invoice_one.sales_invoice_one.get_sucursal",
				callback: function (r) {
					if (!r.exc) {
						frm.set_value("sucursal", r.message[0]);
						frm.set_value("dir_sucursal", r.message[1])
					}
				}
			});
		}
	},
	no_id: function (frm) {
		if (frm.doc.no_id) {
			frappe.db.get_value("Customer", {
				"tax_id": frm.doc.no_id
			}, ["customer_name", "phone", "mobile"], function (r) {
				if (typeof (r) == "undefined") {
					console.log("no hay");
					frappe.confirm(
						"El cliente no existe DESEA CREARLO",
						function () {
							var id = frm.doc.no_id;
							frappe.set_route("Form", "Customer", "Nuevo Cliente 1", {
								"tax_id": frm.doc.no_id
							});
							frm.set_value("no_id", id)
						},
						function () {
							show_alert('NO SE ENCONTRARON COINCIDENSIAS')
						}
					)
				} else {
					frm.set_value("customer", r.customer_name);
				}
			})
			frm.set_value("customer_name", frm.doc.customer);
			frm.set_value("due_date", frappe.datetime.nowdate());
		}

		frm.refresh_fields();

	},

	refresh: function (frm) {
		if (frm.doc.nota_venta == 1) {
			console.log(frm.doc.naming_series);
			frm.set_value("naming_series", "NV-002-.#########");
		}
		frappe.db.get_value("Punto de Venta", {}, "sequence_note", function (r) {
			console.log(r.sequence_note);
		})
		if (frm.doc.status == 'Draft' && frm.doc.docstatus == '0') {
			frm.add_custom_button(__("Pagar"), function () {
				frm.events.update_to_pay_sale(frm);
			}).addClass("btn-primary");
		}

		if (frm.doc.status == 'Confirmed' && frm.doc.docstatus == '0') {
			frm.add_custom_button(__("Pagar"), function () {
				frm.events.update_to_pay_sale(frm);
			}).addClass("btn-primary");
		}

		if (frm.doc.status == 'Confirmed' && frm.doc.docstatus == '1') {
			frm.add_custom_button(__("Pagar"), function () {
				frm.events.update_to_pay_sale(frm);
			}).addClass("btn-primary");
		}

		if (frm.doc.status == 'Done' && frm.doc.docstatus == '1') {
			frm.add_custom_button(__("Anular"), function () {
				frm.events.update_to_anulled_sale(frm);
			}).addClass("btn-primary");
		}
		frm.refresh_fields();
	},
	cod_item: function (frm, cdt, cdn) {
		var d = frm.doc;
		var barcode = "";
		if (d.cod_item) {
			if (d.items.length == 1) {
				$.each(d.items, function (index, data) {
					if (data.item_name) {} else {
						remove_child_ref(frm);
					}
				})
			}
			if (d.items.length >= 1) {
				re_tab(frm);
			} else {
				add_child_tab(frm);
			}
		}
		frm.set_value("cod_item", "")
	},
	customer: function (frm) {
		if (frm.doc.customer) {
			frappe.db.get_value("Customer", {
				"customer_name": frm.doc.customer
			}, "tax_id", function (r) {
				frm.set_value("no_id", r.tax_id);
			})
			frappe.db.get_value("Customer", {
				"customer_name": frm.doc.customer
			}, ["email", "phone", "mobile"], function (r) {
				frm.set_value("correo_cliente", r.email);
				if (r.mobile) {
					console.log("ingresa");
					frm.set_value("num_telefono", r.mobile);

				} else {
					frm.set_value("num_telefono", r.phone);
				}
			})

			frm.set_value("customer_name", frm.doc.customer);
			frm.set_value("due_date", frappe.datetime.nowdate());
		}
		frm.refresh_fields();
	},

	nota_venta: function (frm) {

		return frappe.call({
			doc: frm.doc,
			method: "get_series",
			args: {
				"nota_venta": frm.doc.nota_venta
			},
			callback: function (r) {
				if (!r.exc) {
					//frm.set_value("naming_series", r.message);
					set_field_options("naming_series", r.message);
				}
				refresh_field("naming_series");
				frm.refresh_fields();
				frm.refresh();
			}
		});
	},

	update_to_quotation_sale: function (frm) {
		return frappe.call({
			doc: frm.doc,
			method: "update_to_quotation_sale",
			freeze: true,
			callback: function (r) {
				frm.refresh_fields();
				frm.refresh();
			}
		})
	},

	update_to_anulled_sale: function (frm) {
		var d = new frappe.ui.Dialog({
			title: __("Esta seguro de Anular la Venta"),
			fields: [{
					fieldname: "NO",
					"label": __("No"),
					"fieldtype": "Button"
				},
				{
					"fieldname": "coulmn_break_2",
					"fieldtype": "Column Break"
				},
				{
					fieldname: "SI",
					"label": __("Si"),
					"fieldtype": "Button"
				}
			]
		});

		d.get_input("NO").on("click", function () {
			d.hide();
		});

		d.get_input("SI").on("click", function () {
			return frappe.call({
				doc: frm.doc,
				method: "update_to_anulled_sale",
				freeze: true,
				callback: function (r) {
					d.hide();
					frm.refresh_fields();
					frm.refresh();
				}
			})
		});
		d.show();
		frm.refresh_fields();
		frm.refresh();

	},

	update_to_pay_sale: function (frm) {
		var me = this;
		if (frm.doc.posting_date != frm.doc.due_date && frm.doc.status == "Draft") {
			total = 0.0
		} else {
			total = frm.doc.residual_amount
		}

		var d = new frappe.ui.Dialog({
			title: __("Payment"),
			fields: [{
					"fieldname": "customer",
					"fieldtype": "Link",
					"label": __("Customer"),
					options: "Customer",
					reqd: 1,
					label: "Customer",
					"default": frm.doc.customer_name
				},
				{
					"fieldname": "total",
					"fieldtype": "Currency",
					"label": __("Total Amount"),
					label: "Total Amount",
					"default": total
				},
				{
					"fieldname": "section_break",
					"fieldtype": "Section Break"
				},
				{
					"fieldname": "recibo",
					"fieldtype": "Currency",
					"label": __("RECIBIDO"),
					label: "RECIBIDO",
					"default": frm.doc.residual_amount
				},
				{
					"fieldname": "coulmn_break_2",
					"fieldtype": "Column Break"
				},
				{
					"fieldname": "cambio",
					"fieldtype": "Read Only",
					"label": __("CAMBIO"),
					label: "CAMBIO",
					"default": 0
				},
				{
					"fieldname": "section_break",
					"fieldtype": "Section Break"
				},
				{
					fieldname: "pay",
					"label": __("Pay"),
					"fieldtype": "Button"
				}
			]
		});

		d.get_input("recibo").on("change", function () {
			var values = d.get_values();
			var cambio = flt(values["recibo"] - values["total"])
			cambio = Math.round(cambio * 100) / 100
			d.set_value("cambio", cambio)
		});

		d.get_input("pay").on("click", function () {
			var values = d.get_values();
			if (!values) return;
			total = Math.round(values["total"] * 100) / 100
			total_pay = Math.round(frm.doc.total * 100) / 100

			if (total < 0) frappe.throw("Ingrese monto a pagar");
			if (total < 0) frappe.throw("Monto a pagar no puede ser menor a 0");

			if (total > total_pay) frappe.throw("Monto a pagar no puede ser mayor al monto total de venta");
			return frappe.call({
				doc: frm.doc,
				method: "update_to_pay_sale",
				args: values,
				freeze: true,
				callback: function (r) {
					d.hide();
					refresh_field("payments");
					frm.refresh_fields();
					frm.refresh();
				}
			})
		});

		d.show();
		refresh_field("payments");
		frm.refresh_fields();
		frm.refresh();
	},

});

frappe.ui.form.on('Sales Invoice Item One', {
	item_name: function (frm, cdt, cdn) {
		var item = frappe.get_doc(cdt, cdn);
		if (!item.item_name) {
			item.item_name = "";
		} else {
			item.item_name = item.item_name;
		}
	},
	descuento: function (frm, cdt, cdn) {
		console.log("desde descuento");

		var item = frappe.get_doc(cdt, cdn);
		item.unit_price = item.precio_descuento_fijado;
		item.unit_price_out_dcto = item.precio_descuento_fijado;
		refresh_field("items");
		cur_frm.refresh_fields();
		var d = locals[cdt][cdn];
		if (d.descuento == 1) {
			if (d.item_code) {
				d.unit_price_with_tax = d.precio_descuento_fijado;
				d.unit_price_out_dcto = d.precio_descuento_fijado / 1.12;
				d.unit_price = d.unit_price_out_dcto;
				d.subtotal_imp = d.unit_price_with_tax * d.qty;
				d.subtotal = d.unit_price * d.qty;
				refresh_field("items");
				cur_frm.refresh_fields();
				calculate_base_imponible(frm)
			}
		} else {

			args = {
				'item_code': d.item_code,
				'qty': d.qty
			};
			// console.log(">>>>" d.qty);
			return frappe.call({
				doc: cur_frm.doc,
				method: "get_item_details_sale",
				args: args,
				callback: function (r) {
					if (r.message) {
						var d = locals[cdt][cdn];
						$.each(r.message, function (k, v) {
							if (k == "unit_price") {
								d[k] = v;
							}
							if (k == "unit_price_with_tax") {
								d[k] = v;
							}
						});
						d.unit_price_out_dcto = d.unit_price;
						d.subtotal = d.unit_price * d.qty;
						d.subtotal_imp = d.unit_price_with_tax * d.qty;
						refresh_field("items");
						cur_frm.refresh_fields();
						calculate_base_imponible(frm)
					}
				}
			});
			//
		}

	},
	caja: function (frm, cdt, cdn) {
		var d = locals[cdt][cdn];
		if (d.caja == 1) {
			d.num_caja = 1;
			frappe.db.get_value("Item", {
				"barcode": d.barcode
			}, ["check_caja_fras", "cantidad"], function (r) {
				if (r.check_caja_fras == 1) {
					console.log("si tiene", r.cantidad);
					d.qty = r.cantidad
					d.subtotal = d.qty * d.unit_price;
					d.subtotal_imp = d.qty * d.unit_price_with_tax;
					refresh_field("items");
					cur_frm.refresh_fields();
					calculate_base_imponible(frm);
				} else {
					console.log("no esta configurado");
				}
			})
		} else {
			d.qty = 1;
			d.subtotal = d.qty * d.unit_price;
			d.subtotal_imp = d.qty * d.unit_price_with_tax;
			refresh_field("items");
			cur_frm.refresh_fields();
			calculate_base_imponible(frm);
		}
		refresh_field("items");
		cur_frm.refresh_fields();
		calculate_base_imponible(frm);
	},
	num_caja: function (frm, cdt, cdn) {
		var d = locals[cdt][cdn];
		if (d.num_caja >= 1) {
			frappe.db.get_value("Item", {
				"barcode": d.barcode
			}, ["check_caja_fras", "cantidad", "precio_2_con_imp", "precio_2_sin_imp"], function (r) {
				console.log(r.cantidad * d.num_caja);
				d.qty = r.cantidad * d.num_caja
				if (d.qty >= r.cantidad) {
					d.unit_price = r.precio_2_sin_imp;
					d.unit_price_with_tax = r.precio_2_con_imp;
					d.subtotal = d.qty * d.unit_price;
					d.subtotal_imp = d.qty * d.unit_price_with_tax;
					refresh_field("items");
					cur_frm.refresh_fields();
					calculate_base_imponible(frm);
				} else {
					d.subtotal = d.qty * d.unit_price;
					d.subtotal_imp = d.qty * d.unit_price_with_tax;
					refresh_field("items");
					cur_frm.refresh_fields();
					calculate_base_imponible(frm);
				}
			})
		}
		refresh_field("items");
		cur_frm.refresh_fields();
		calculate_base_imponible(frm);
	},


	barcode: function (frm, cdt, cdn) {
		var d = locals[cdt][cdn];
		if (d.barcode) {
			args = {
				'barcode': d.barcode
			};
			return frappe.call({
				doc: cur_frm.doc,
				method: "get_item_code_sale",
				args: args,
				callback: function (r) {
					if (r.message) {
						var d = locals[cdt][cdn];
						$.each(r.message, function (k, v) {
							d[k] = v;
						});
						refresh_field("items");
						cur_frm.refresh_fields();
						calculate_base_imponible(frm);
					}
				}
			});
		}
		cur_frm.refresh_fields();
	},

	item_code: function (frm, cdt, cdn) {
		var d = locals[cdt][cdn];
		var base_imponible = 0;
		var total_taxes = 0;
		var total = 0;
		var doc = frm.doc;

		if (d.item_code) {
			args = {
				'item_code': d.item_code,
				'qty': d.qty
			};
			return frappe.call({
				doc: cur_frm.doc,
				method: "get_item_details_sale",
				args: args,
				callback: function (r) {
					if (r.message) {
						var d = locals[cdt][cdn];
						$.each(r.message, function (k, v) {
							d[k] = v;
						});
						refresh_field("items");
						cur_frm.refresh_fields();
						calculate_base_imponible(frm)
					}
				}
			});

			frm.refresh_fields();
		}
	},

	uom: function (frm, cdt, cdn) {
		var d = locals[cdt][cdn];

		if (d.uom) {
			args = {
				'item_code': d.item_code,
				'uom': d.uom,
				'stock_uom': d.stock_uom,
				'conversion_factor': d.conversion_factor,
				'unit_price': d.unit_price,
				'qty': d.qty,
			};

			return frappe.call({
				doc: cur_frm.doc,
				method: "update_unit",
				args: args,
				callback: function (r) {
					if (r.message) {
						var d = locals[cdt][cdn];
						$.each(r.message, function (k, v) {
							d[k] = v;
						});
						calculate_base_imponible(frm);
						refresh_field("items");
						cur_frm.refresh_fields();
					}
				}
			});
		}

	},

	subtotal_imp: function (frm, cdt, cdn) {
		var d = locals[cdt][cdn];

		if (d.uom) {
			args = {
				'item_code': d.item_code,
				'subtotal_imp': d.subtotal_imp,
				'qty': d.qty,
				'discount': d.discount,
				'unit_price': d.unit_price,
			};

			return frappe.call({
				doc: cur_frm.doc,
				method: "calculate_desglose",
				args: args,
				callback: function (r) {
					if (r.message) {
						var d = locals[cdt][cdn];
						$.each(r.message, function (k, v) {
							d[k] = v;
						});
						calculate_base_imponible(frm);
						refresh_field("items");
						cur_frm.refresh_fields();
					}
				}
			});
		}

	},


	conversion_factor: function (frm, cdt, cdn) {
		var d = locals[cdt][cdn];

		if (d.uom) {
			args = {
				'item_code': d.item_code,
				'uom': d.uom,
				'stock_uom': d.stock_uom,
				'conversion_factor': d.conversion_factor,
				'unit_price': d.unit_price,
				'qty': d.qty,
			};

			return frappe.call({
				doc: cur_frm.doc,
				method: "update_unit",
				args: args,
				callback: function (r) {
					if (r.message) {
						var d = locals[cdt][cdn];
						$.each(r.message, function (k, v) {
							d[k] = v;
						});
						calculate_base_imponible(frm);
						refresh_field("items");
						cur_frm.refresh_fields();
					}
				}
			});
		}
	},

	qty: function (frm, cdt, cdn) {
		var d = locals[cdt][cdn];
		frappe.db.get_value("Item", {
			"barcode": d.barcode
		}, ["check_caja_fras", "cantidad"], function (r) {
			if (d.qty >= r.cantidad) {
				d.num_caja = d.qty / r.cantidad;
				args = {
					'item_code': d.item_code,
					'qty': d.qty,
					'unit_price': d.unit_price
				};
				return frappe.call({
					doc: cur_frm.doc,
					method: "update_prices_qty",
					args: args,
					callback: function (r) {
						if (r.message) {
							var d = locals[cdt][cdn];
							$.each(r.message, function (k, v) {
								d[k] = v;
							});
							refresh_field("items");
							cur_frm.refresh_fields();
							calculate_base_imponible(frm);
						}
					}
				});
			}
		})
	},

	unit_price: function (frm, cdt, cdn) {
		// if user changes the rate then set margin Rate or amount to 0
		var d = locals[cdt][cdn];
		if (d.item_code) {
			args = {
				'item_code': d.item_code,
				'qty': d.qty,
				'unit_price': d.unit_price,
				'discount': d.discount,
			};
			return frappe.call({
				doc: cur_frm.doc,
				method: "update_prices_sale",
				args: args,
				callback: function (r) {
					if (r.message) {
						var d = locals[cdt][cdn];
						$.each(r.message, function (k, v) {
							d[k] = v;
						});
						refresh_field("items");
						cur_frm.refresh_fields();
						calculate_base_imponible(frm);
					}
				}
			});
		}
	},

	discount: function (frm, cdt, cdn) {
		var d = locals[cdt][cdn];
		d.unit_price = d.unit_price_out_dcto;
		if (d.item_code) {
			args = {
				'item_code': d.item_code,
				'qty': d.qty,
				'unit_price': d.unit_price,
				'discount': d.discount,
			};
			return frappe.call({
				doc: cur_frm.doc,
				method: "update_prices_sale",
				args: args,
				callback: function (r) {
					if (r.message) {
						var d = locals[cdt][cdn];
						$.each(r.message, function (k, v) {
							d[k] = v;
						});
						refresh_field("items");
						cur_frm.refresh_fields();
						calculate_base_imponible(frm);
					}
				}
			});
		}
	},

	unit_price_out_dcto: function (frm, cdt, cdn) {
		var item = frappe.get_doc(cdt, cdn);
		item.unit_price = item.unit_price_out_dcto;
		refresh_field("items");
		cur_frm.refresh_fields();

		var d = locals[cdt][cdn];
		if (d.item_code) {
			args = {
				'item_code': d.item_code,
				'qty': d.qty,
				'unit_price': d.unit_price,
				'discount': d.discount,
			};
			return frappe.call({
				doc: cur_frm.doc,
				method: "update_prices_sale",
				args: args,
				callback: function (r) {
					if (r.message) {
						var d = locals[cdt][cdn];
						$.each(r.message, function (k, v) {
							d[k] = v;
						});
						calculate_base_imponible(frm);
						refresh_field("items");
						cur_frm.refresh_fields();
					}
				}
			});
		}
	}
})

var calculate_base_imponible = function (frm) {
	var doc = frm.doc;
	doc.base_imponible = 0;
	doc.total_taxes = 0;
	doc.total = 0;

	if (doc.items) {
		$.each(doc.items, function (index, data) {
			doc.base_imponible += (data.unit_price * data.qty);
			doc.total_taxes += (data.unit_price_with_tax - data.unit_price) * data.qty;
		})
		doc.base_imponible = Math.round(doc.base_imponible * 100) / 100
		doc.total_taxes = Math.round(doc.total_taxes * 100) / 100
		doc.total += doc.base_imponible + doc.total_taxes;
		doc.total = Math.round(doc.total * 100) / 100
	}


	doc.residual_amount = doc.total;
	refresh_field('residual_amount')
	refresh_field('base_imponible')
	refresh_field('total_taxes')
	refresh_field('total')
}
var remove_child_ref = function (frm) {
	cur_frm.clear_table("items");
};
var re_tab = function (frm) {
	var d = frm.doc;
	var line = 0;
	var i = d.items.length;
	var l = 1;
	while (i--) {
		if (d.items[i].barcode == d.cod_item && i >= 0) {
			console.log(">>", l);
			l = 0;
		}
	}
	console.log(">>>>>>++++", l);
	if (l == 1) {
		console.log("no hay", i);
		add_child_tab(frm);
	} else {
		$.each(d.items, function (index, data) {
			if (data.barcode == d.cod_item) {
				data.qty = data.qty + 1;
				data.unidades = data.qty;
				data.subtotal = data.unit_price_out_dcto * data.qty;
				data.subtotal_imp = data.unit_price_with_tax * data.qty;
				refresh_field("items");
				d.cod_item = "";
				refresh_field("cod_item");
				frm.refresh_fields();
				calculate_base_imponible(frm);
			}
		})
	}
};
var add_child_tab = function (frm) {
	var d = frm.doc;
	var qty = 1;
	var long = 0;
	console.log("ingresa add_child");
	frappe.db.get_value("Item", {
		"barcode": d.cod_item
	}, ["item_code", "item_name", "precio_con_descuento", "list_price_with_tax", "item_group", "list_price", "barcode"], function (r) {
		if (r.item_code) {
			var new_row = frm.add_child("items");
			new_row.item_code = r.item_code;
			new_row.barcode = r.barcode;
			new_row.item_name = r.item_name;
			new_row.unit_price_out_dcto = r.list_price;
			new_row.unit_price = r.list_price;
			new_row.discount = 0.00;
			new_row.unit_price_with_tax = r.list_price_with_tax;
			new_row.qty = 1;
			new_row.unidades = 1;
			new_row.subtotal = r.list_price * 1;
			new_row.uom = "Unidad(es)";
			new_row.conversion_factor = 1.00;
			new_row.precio_descuento_fijado = r.precio_con_descuento;
			new_row.subtotal_imp = r.list_price_with_tax * 1;
			refresh_field("items");
			frm.refresh_fields();
			calculate_base_imponible(frm);
			frm.set_value("cod_item", "")
			refresh_field("cod_item");
		}
		refresh_field("items");
		frm.refresh_fields();
	})

};